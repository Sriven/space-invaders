import pygame


class spaceship(object):

    def __init__(self, dw, dh):

        self.img = pygame.image.load('galaga-ship2.png')
        self.y = ((dh * 7) / 8)
        self.x = (dw * (0/8))

    def ship(self, surface):
        surface.blit(self.img, (self.x, self.y))

    def right(self):
        if self.x != 560:
            self.x += 80

    def left(self):
        if self.x != 0:
            self.x -= 80
