from missiles import *


class missile2(missiles):

    def __init__(self, surface, col):
        super(missile2, self).__init__(surface, "2bullet.png", col)

    def collision2(self, listalien, surface):

        for alien in listalien:
            if abs(alien.alien_pos_x - self.pos_x) < 40:
                if self.pos_y - alien.alien_pos_y < 30:
                    alien.alienhit2(surface)
                    self.dead = True
                    return self.dead
