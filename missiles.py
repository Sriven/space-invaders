import pygame


class missiles(object):

    def __init__(self, surface, imgloc, col):

        self.missileimg = pygame.image.load(imgloc)
        self.missileimg = pygame.transform.scale(self.missileimg, (60, 60))
        self.pos_x = col + 8
        self.pos_y = 480
        self.dead = False
        surface.blit(self.missileimg, (480, col + 8))

    def updatemissile(self, surface, speed):

        self.missilekill()
        self.pos_y = self.pos_y - speed
        surface.blit(self.missileimg, (self.pos_x, self.pos_y))
        return self.dead

    def missilekill(self):

        if self.pos_y < -80:
            self.dead = True
