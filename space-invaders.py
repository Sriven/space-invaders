import pygame
from missiles import *
from random import *
from aliens import *
from missile1 import *
from missile2 import *
from spaceship import *

pygame.init()

display_width = 640
display_height = 640

# row_random = [600, 540]
# col_random = [0, 60, 120, 180, 240, 300, 360, 420, 480, 540]
alien = []
missiles1 = []
missiles2 = []
deletemissile1 = []
deletemissile2 = []
deletealien = []
score = 0

surface = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('Space Invaders (Not Yet)')

black = (0, 0, 0)
white = (255, 255, 255)

fps = pygame.time.Clock()
stop = False
conflict = True

pygame.time.set_timer(pygame.USEREVENT+1, 10000)
pygame.time.set_timer(pygame.USEREVENT+2, 10)

alien.append(aliens(surface, randint(0, 7) * 80, randint(0, 1) * 80))
ship = spaceship(display_width, display_height)


def display_score():
    scorefont = pygame.font.SysFont('com.ttf', 30)
    scoretext = scorefont.render("Score = " + str(score), True, white)
    score_rect = scoretext.get_rect()
    score_rect.center = ((display_width/2), (display_height/2))
    surface.blit(scoretext, score_rect)


while not stop:

    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

        if event.type == pygame.KEYUP:

            if event.key == pygame.K_q:
                pygame.quit()
                quit()

            if event.key == pygame.K_d:
                ship.right()

            if event.key == pygame.K_a:
                ship.left()

            if event.key == pygame.K_s:
                missiles2.append(missile2(surface, ship.x))

            if event.key == pygame.K_SPACE:
                missiles1.append(missile1(surface, ship.x))

            if event.type == pygame.USEREVENT+2:
                pass

        if event.type == pygame.USEREVENT+1:
            rand_x = randint(0, 7) * 80
            rand_y = randint(0, 1) * 80
            conflict = True

            while conflict:

                rand_x = randint(0, 7) * 80
                rand_y = randint(0, 1) * 80
                conflict = False

                for alalien in alien:
                    px = alalien.alien_pos_x
                    py = alalien.alien_pos_y

                    if px == rand_x or py == rand_y:
                        conflict = True

            alien.append(aliens(surface, rand_x, rand_y))

        surface.fill(black)
        ship.ship(surface)

        for i in range(len(alien)):
            # print "updating aliens at", time.time()
            flag = alien[i].alienupdate(surface)

            if flag == True:
                deletealien.append(i)
                flag = False

        for i in range(len(missiles1)):
            flag = missiles1[i].updatemissile(surface, 0.8)
            flag = missiles1[i].collision1(alien, surface)

            if flag == True:
                deletemissile1.append(i)
                score += 10
                flag = False

        for i in range(len(missiles2)):
            flag = missiles2[i].updatemissile(surface, 1.6)
            flag = missiles2[i].collision2(alien, surface)

            if flag == True:
                deletemissile2.append(i)
                flag = False

        count = 0
        for i in deletemissile1:
            del missiles1[i - count]
            count += 1

        count = 0
        for i in deletemissile2:
            del missiles2[i - count]
            count += 1

        count = 0
        for i in deletealien:
            del alien[i - count]
            count += 1

        if missiles1 and missiles1[0].dead:
            missiles1.pop(0)

        if missiles2 and missiles2[0].dead:
            missiles2.pop(0)

        deletemissile2 = []
        deletemissile1 = []
        deletealien = []

    # print "Missile1 list = ", missiles1
    # print "Missile2 list = ", missiles2
    # print "alien list = ", alien
    # print "Score = ", score

    display_score()

    pygame.display.flip()
    fps.tick(60)

pygame.quit()
quit()
