import pygame
import time


class aliens():

    def __init__(self, surface, position_x, position_y):

        self.alienimg = pygame.image.load('alien.png')
        self.alien_pos_x = position_x
        self.alien_pos_y = position_y
        self.start_time = time.time()
        self.dead = False
        surface.blit(self.alienimg, (position_x, position_y))

    def alienupdate(self, surface):

        self.end_time = time.time()

        if (self.end_time - self.start_time) > 8:
            self.dead = True
        else:
            # alienimg = pygame.image.load('alien.png')
            surface.blit(self.alienimg, (self.alien_pos_x, self.alien_pos_y))

        return self.dead

    def alienhit2(self, surface):

        self.start_time = time.time() - 3
        self.alienimg = pygame.image.load('alienhit.png')
        surface.blit(self.alienimg, (self.alien_pos_x, self.alien_pos_y))

    def alienhit1(self, surface):

        self.dead = True
