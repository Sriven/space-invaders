from missiles import *


class missile1(missiles):

    def __init__(self, surface, col):
        super(missile1, self).__init__(surface, "1bullet.png", col)

    def collision1(self, listalien, surface):

        for alien in listalien:
            if abs(alien.alien_pos_x - self.pos_x) < 40:
                if (self.pos_y - alien.alien_pos_y) < 40:
                    alien.alienhit1(surface)
                    self.dead = True
                    return self.dead
                    break
